﻿using UnityEngine;
using UnityEngine.Events;

public class ActionTrigger : MonoBehaviour
{
    public LayerMask Mask;
    public UnityEvent OnActionTriggered;

    void OnTriggerEnter(Collider other)
    {
        if (!Layers.Masks.CheckLayer(other.gameObject.layer, Mask.value)) return;

        OnActionTriggered.Invoke();
    }
}
