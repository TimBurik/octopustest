﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryStorageData
{
    private readonly Dictionary<ItemType, int> _storageData;

    public InventoryStorageData()
    {
        _storageData = new Dictionary<ItemType, int>();
    }

    public void AddItem(ItemType type, int amount)
    {
        if (_storageData.ContainsKey(type))
        {
            _storageData[type] += amount;
        }
        else
        {
            _storageData.Add(type, amount);
        }
    }

    public void RemoveItem(ItemType type, int amount)
    {
        if (_storageData.ContainsKey(type))
        {
            _storageData[type] = Mathf.Max(0, _storageData[type] - amount);
        }
    }

    public int GetItemAmount(ItemType type)
    {
        if (!_storageData.ContainsKey(type))
        {
            _storageData.Add(type, 0);
        }
        return _storageData[type];
    }
}
