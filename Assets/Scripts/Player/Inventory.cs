﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ItemRecord
{
    public ItemType Type;
    public int Amount;
}

public class Inventory : MonoBehaviour
{
    public List<ItemRecord> InitialInventoryContent;

    private InventoryStorageData _inventoryData;

    public void Init()
    {
        _inventoryData = GameSessionStorage.Instance.InventoryData;
        if (_inventoryData == null)
        {
            _inventoryData = new InventoryStorageData();
            foreach (ItemRecord itemRecord in InitialInventoryContent)
            {
                _inventoryData.AddItem(itemRecord.Type, itemRecord.Amount);
            }
            GameSessionStorage.Instance.SaveInventoryData(_inventoryData);
        }
    }

    public void AddItem(ItemType type, int amount)
    {
        _inventoryData.AddItem(type, amount);
    }

    public void RemoveItem(ItemType type, int amount)
    {
        _inventoryData.RemoveItem(type, amount);
    }

    public int GetItemAmount(ItemType type)
    {
        return _inventoryData.GetItemAmount(type);
    }
}