﻿using System.Collections.Generic;
using UnityEngine;

public class DecalPool : MonoBehaviour
{
    private readonly Dictionary<GameObject, List<DecalBase>> _decalPool = new Dictionary<GameObject, List<DecalBase>>();

    public DecalBase Get(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        if (!_decalPool.ContainsKey(prefab))
        {
            _decalPool.Add(prefab, new List<DecalBase>());
        }
        if (_decalPool[prefab].Count > 0)
        {
            DecalBase instanciated = _decalPool[prefab][0];
            _decalPool[prefab].RemoveAt(0);
            instanciated.Move(position, rotation);
            instanciated.GameObject.SetActive(true);
            return instanciated;
        }
        else
        {
            DecalBase instanciatedDecal = Create(prefab);
            instanciatedDecal.Move(position, rotation);
            return instanciatedDecal;
        }
    }

    public void Release(DecalBase decal)
    {
        decal.GameObject.SetActive(false);
        _decalPool[decal.Prefab].Add(decal);
    }

    private DecalBase Create(GameObject prefab)
    {
        GameObject instanciatedGameObject = Instantiate(prefab);
        DecalBase instanciatedDecal = instanciatedGameObject.GetComponent<DecalBase>();
        instanciatedDecal.Init(prefab);
        return instanciatedDecal;
    }
}
