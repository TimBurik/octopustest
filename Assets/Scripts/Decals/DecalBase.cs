﻿using System;
using UnityEngine;

public class DecalBase : MonoBehaviour
{
    public event Action<DecalBase> OnReleased;

    private GameObject _gameObject;
    public GameObject GameObject
    {
        get
        {
            if (_gameObject == null)
            {
                _gameObject = gameObject;
            }
            return _gameObject;
        }
    }

    private Transform _transform;
    public Transform Transform
    {
        get
        {
            if (_transform == null)
            {
                _transform = GetComponent<Transform>();
            }
            return _transform;
        }
    }

    private DecalPool _decalPool;
    public DecalPool DecalPool
    {
        get
        {
            if (_decalPool == null)
            {
                _decalPool = SceneMediator.Instance.GetManager<DecalPool>();
            }
            return _decalPool;
        }
    }

    public GameObject Prefab { get; private set; }

    public void Init(GameObject prefab)
    {
        Prefab = prefab;
    }

    public void Move(Vector3 position, Quaternion rotation)
    {
        Transform.position = position;
        Transform.rotation = rotation;
    }

    public void Release()
    {
        if (OnReleased != null)
        {
            OnReleased.Invoke(this);
        }

        Reset();
        DecalPool.Release(this);
    }

    protected virtual void Reset()
    {
        OnReleased = null;
    }
}
