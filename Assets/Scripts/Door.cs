﻿using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    public List<PlayerHasItemsCondition> OpenConditions;

    public Renderer MainRenderer;
    public Material Normal;
    public Material Hover;

    private Animator _doorAnimator;
    public Animator DoorAnimator
    {
        get
        {
            if (_doorAnimator == null)
            {
                _doorAnimator = GetComponent<Animator>();
            }
            return _doorAnimator;
        }
    }

    public bool IsOpened
    {
        get { return _doorIsOpened; }
    }
    public bool CanOpen
    {
        get { return OpenConditions.TrueForAll(condition => condition.Check().Satisfied); }
    }

    private MaterialSwapHover _hoverLogic;
    private bool _doorIsOpened;

    void Start()
    {
        _hoverLogic = new MaterialSwapHover(MainRenderer, Normal, Hover);
    }

    public void OpenDoor()
    {
        if (!_doorIsOpened)
        {
            DoorAnimator.Play("DoorOpening");
            _doorIsOpened = true;
        }
    }

    public void CloseDoor()
    {
        if (_doorIsOpened)
        {
            DoorAnimator.Play("DoorClosing");
            _doorIsOpened = false;
        }
    }

    public string HintMessage
    {
        get { return "[RClick] to open door"; }
    }

    public void Interact()
    {
        if (CanOpen)
        {
            OpenDoor();
            SceneMediator.Instance.GetManager<GameLogic>().OnDoorOpened(this);
        }
    }

    public void OnHoverEnter()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnDoorHoverEnter(this);

        _hoverLogic.OnHoverEnter();
    }

    public void OnHoverExit()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnDoorHoverExit(this);

        _hoverLogic.OnHoverExit();
    }

    public void OnDoorPassed()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnDoorEntered(this);
    }
}