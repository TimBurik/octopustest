﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ItemSpawner))]
public class ItemSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        SerializedProperty prefabsList = serializedObject.FindProperty("ItemPrefabs");
        SerializedProperty spawnPointsList = serializedObject.FindProperty("ItemSpawnPoints");

        EditorGUILayout.LabelField("Item Prefabs");
        for (int i = 0; i < prefabsList.arraySize; i++)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            SerializedProperty prefab = prefabsList.GetArrayElementAtIndex(i);
            prefab.objectReferenceValue = EditorGUILayout.ObjectField(prefab.objectReferenceValue, typeof(GameObject), false, GUILayout.MaxWidth(Screen.width - 30.0f)) as GameObject;

            EditorGUILayout.Separator();
            if (GUILayout.Button("X", GUILayout.MaxWidth(30.0f)))
            {
                prefabsList.GetArrayElementAtIndex(i).objectReferenceValue = null;
                prefabsList.DeleteArrayElementAtIndex(i);
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Separator();

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Add", GUILayout.MaxWidth(50.0f)))
        {
            prefabsList.InsertArrayElementAtIndex(prefabsList.arraySize);
            prefabsList.GetArrayElementAtIndex(prefabsList.arraySize - 1).objectReferenceValue = null;
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Spawn Points");
        for (int i = 0; i < spawnPointsList.arraySize; i++)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            SerializedProperty spawnPoint = spawnPointsList.GetArrayElementAtIndex(i);
            spawnPoint.objectReferenceValue = EditorGUILayout.ObjectField(spawnPoint.objectReferenceValue, typeof(ItemSpawnPoint), true, GUILayout.MaxWidth(Screen.width - 30.0f)) as ItemSpawnPoint;

            EditorGUILayout.Separator();
            if (GUILayout.Button("X", GUILayout.MaxWidth(30.0f)))
            {
                spawnPointsList.GetArrayElementAtIndex(i).objectReferenceValue = null;
                spawnPointsList.DeleteArrayElementAtIndex(i);
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Separator();

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Find all", GUILayout.MaxWidth(100.0f)))
        {
            ItemSpawner spawner = serializedObject.targetObject as ItemSpawner;
            spawnPointsList.ClearArray();
            foreach (ItemSpawnPoint spawnPoint in spawner.FindSpawnPointsInLoaction())
            {
                spawnPointsList.InsertArrayElementAtIndex(spawnPointsList.arraySize);
                spawnPointsList.GetArrayElementAtIndex(spawnPointsList.arraySize - 1).objectReferenceValue =
                    spawnPoint;
            }
        }
        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
