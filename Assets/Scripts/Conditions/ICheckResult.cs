﻿public interface ICheckResult
{
    bool Satisfied { get; }
    string Message { get; }
}
