﻿public interface ICondition
{
    ICheckResult Check();
}
