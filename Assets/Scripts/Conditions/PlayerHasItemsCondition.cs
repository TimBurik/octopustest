﻿using System;

[Serializable]
public class PlayerHasItemsCondition : ICondition
{
    public ItemType Type;
    public int MinAmount;

    public ICheckResult Check()
    {
        PlayerHasItemsCheckResult result = new PlayerHasItemsCheckResult();
        int availableAmount = SceneMediator.Instance.Player.Inventory.GetItemAmount(Type);
        if (availableAmount < MinAmount)
        {
            result.ItemShortage(Type, MinAmount - availableAmount);
        }
        return result;
    }
}
