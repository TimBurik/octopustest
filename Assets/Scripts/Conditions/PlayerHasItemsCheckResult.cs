﻿public class PlayerHasItemsCheckResult : ICheckResult
{
    public bool Satisfied { get; private set; }
    public string Message { get; private set; }

    public PlayerHasItemsCheckResult()
    {
        Satisfied = true;
        Message = "";
    }

    public void ItemShortage(ItemType type, int neededAmount)
    {
        Satisfied = false;
        Message = string.Format("Need {0} more items of type {1}", neededAmount, type);
    }
}
