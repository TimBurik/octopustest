﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions{
    public static void Shuffle<T>(this List<T> list){
        for (int i = 0; i < list.Count; i++){
            int indexToSwap = Random.Range(0, list.Count);

            var firstElement = list[i];
            list[i] = list[indexToSwap];
            list[indexToSwap] = firstElement;
        }
    }

    public static T GetRandomElement<T>(this List<T> list){
        int index = Random.Range(0, list.Count);
        return list[index];
    }
}