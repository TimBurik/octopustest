﻿using UnityEngine;

public class CameraClipping : MonoBehaviour
{
    public Transform PivotTransform;
    public float ReserveGap;

    private Transform _transform;
    public Transform Transform
    {
        get
        {
            if (_transform == null)
            {
                _transform = GetComponent<Transform>();
            }
            return _transform;
        }
    }

    private Vector3 _cameraDirection;
    private float _cameraDistance;
    private RaycastHit _raycastInfo;

    void Start()
    {
        _cameraDirection = Transform.localPosition;
        _cameraDistance = _cameraDirection.magnitude;
    }

    void FixedUpdate()
    {
        if (Physics.Raycast(PivotTransform.position, PivotTransform.TransformDirection(_cameraDirection),
            out _raycastInfo, _cameraDistance + ReserveGap, Layers.Masks.CameraClippingDetection))
        {
            Transform.localPosition = _cameraDirection.normalized*(_raycastInfo.distance - ReserveGap);
        }
        else
        {
            Transform.localPosition = _cameraDirection.normalized * _cameraDistance;
        }
    }
}
