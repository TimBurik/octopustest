﻿using UnityEngine;

public class CameraLook : MonoBehaviour
{
    public Transform RotationPivot;
    public float MinimumX;
    public float MaximumX;

    private Transform _transform;
    public Transform Transform
    {
        get
        {
            if (_transform == null)
            {
                _transform = GetComponent<Transform>();
            }
            return _transform;
        }
    }

    private Quaternion _targetCameraLook;

    void Start()
    {
        _targetCameraLook = RotationPivot.localRotation;
    }

    public void RotateCamera(float rotationX, float rotationY)
    {
        _targetCameraLook *= Quaternion.Euler(rotationX, 0f, 0f);

        RotationPivot.localRotation = ClampRotationAroundXAxis(_targetCameraLook);
        Transform.Rotate(Vector3.up, rotationY);
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
