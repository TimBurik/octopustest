﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform _transform;
    public Transform Transform
    {
        get
        {
            if (_transform == null)
            {
                _transform = GetComponent<Transform>();
            }
            return _transform;
        }
    }

    void LateUpdate()
    {
        Transform.position = SceneMediator.Instance.Player.Transform.position;
    }

}