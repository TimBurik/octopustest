﻿using UnityEngine;

public class InteractableDetector : MonoBehaviour
{
    public float DetectionDistance;
    public float DetectionRadius;

    public IInteractable DetectedInteractable { get; private set; }

    private Camera _currentCamera;
    public Camera CurrentCamera
    {
        get
        {
            if (_currentCamera == null)
            {
                _currentCamera = GetComponent<Camera>();
            }
            return _currentCamera;
        }
    }

    private RaycastHit _raycastInfo;

    void FixedUpdate()
    {
        if (Physics.SphereCast(CurrentCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f)), DetectionRadius, out _raycastInfo, DetectionDistance, Layers.Masks.InteractableDetection) 
            && (_raycastInfo.collider.gameObject.layer == Layers.Interactable))
        {
            IInteractable detectionResult = _raycastInfo.collider.GetComponent<IInteractable>();

            if (DetectedInteractable != detectionResult)
            {
                if (DetectedInteractable != null)
                {
                    DetectedInteractable.OnHoverExit();
                }

                DetectedInteractable = detectionResult;
                if (DetectedInteractable != null)
                {
                    DetectedInteractable.OnHoverEnter();
                }
            }
        }
        else
        {
            if (DetectedInteractable != null)
            {
                DetectedInteractable.OnHoverExit();
                DetectedInteractable = null;
            }
        }
    }
}
