﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ActionInputController : MonoBehaviour
{
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonUp("Fire1"))
        {
            SceneMediator.Instance.GetManager<GameLogic>().OnActionInput();
        }
    }
}
