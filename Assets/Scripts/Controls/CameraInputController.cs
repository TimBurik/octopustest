﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CameraInputController : MonoBehaviour
{
    public float XSensitivity = 2f;
    public float YSensitivity = 2f;

    private bool _cursorIsLocked = true;

    void Update()
    {
        float yRot = CrossPlatformInputManager.GetAxis("Mouse X") * XSensitivity;
        float xRot = CrossPlatformInputManager.GetAxis("Mouse Y") * YSensitivity;

        SceneMediator.Instance.FollowCamera.MovementController.RotateCamera(-xRot, yRot);

        CursorLockUpdate();
    }

    private void CursorLockUpdate()
    {
        if (CrossPlatformInputManager.GetButtonDown("Cancel"))
        {
            _cursorIsLocked = false;
        }
        else if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            _cursorIsLocked = true;
        }

        if (_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
