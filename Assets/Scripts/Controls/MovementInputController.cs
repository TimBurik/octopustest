﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MovementInputController : MonoBehaviour
{
    private Transform _cameraTransform;
    public Transform CameraTransform
    {
        get
        {
            if (_cameraTransform == null)
            {
                _cameraTransform = SceneMediator.Instance.FollowCamera.MainCamera.GetComponent<Transform>();
            }
            return _cameraTransform;
        }
    }

    private Vector3 m_CamForward;
    private Vector3 m_Move;
    private bool m_Jump;

    private void Update()
    {
        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
    }

    private void FixedUpdate()
    {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        bool crouch = Input.GetKey(KeyCode.C);

        m_CamForward = Vector3.Scale(CameraTransform.forward, new Vector3(1, 0, 1)).normalized;
        m_Move = v * m_CamForward + h * CameraTransform.right;
       
        SceneMediator.Instance.Player.MovementController.Move(m_Move, crouch, m_Jump);
        m_Jump = false;
    }
}
