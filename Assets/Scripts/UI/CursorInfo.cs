﻿using UnityEngine;
using UnityEngine.UI;

public class CursorInfo : MonoBehaviour
{
    public Text InfoText;

    public void ShowInfoText(string text)
    {
        InfoText.gameObject.SetActive(true);
        InfoText.text = text;
    }

    public void HideInfoText()
    {
        InfoText.gameObject.SetActive(false);
    }
}
