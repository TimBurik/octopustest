﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class InventoryInfo : MonoBehaviour
{
    public Text InventoryLabel;
    public List<ItemType> ShowableItems;

    public void RedrawInventoryInfo(Inventory inventory)
    {
        StringBuilder infoBuilder = new StringBuilder("Inventory\n");
        foreach (ItemType type in ShowableItems)
        {
            infoBuilder.Append(type);
            infoBuilder.Append(" : ");
            infoBuilder.Append(inventory.GetItemAmount(type));
            infoBuilder.Append("\n");
        }
        InventoryLabel.text = infoBuilder.ToString();
    }
}
