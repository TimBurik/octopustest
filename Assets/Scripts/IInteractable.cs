﻿public interface IInteractable
{
    string HintMessage { get; }

    void Interact();
    void OnHoverEnter();
    void OnHoverExit();
}
