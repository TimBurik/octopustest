﻿public static class Layers
{
    public const int Default = 0;
    public const int TransparentFX = 1;
    public const int IgnoreRaycast = 2;
    public const int Water = 4;
    public const int UI = 5;

    public const int Trigger = 29;
    public const int Interactable = 30;
    public const int Player = 31;

    public static class Masks
    {
        public const int InteractableDetection = ~(1 << Player) & ~(1 << IgnoreRaycast) & ~(1 << Trigger);
        public const int CameraClippingDetection = ~(1 << Player) & ~(1 << IgnoreRaycast) & ~(1 << Trigger);

        public static bool CheckLayer(int layer, int mask)
        {
            return ((1 << layer) & mask) != 0;
        }
    }
}