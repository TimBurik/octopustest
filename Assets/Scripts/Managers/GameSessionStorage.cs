﻿public sealed class GameSessionStorage
{
    #region Singleton
    private static GameSessionStorage _instance;
    public static GameSessionStorage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameSessionStorage();
            }
            return _instance;
        }
    }

    private GameSessionStorage()
    {
    }

    #endregion

    public InventoryStorageData InventoryData { get; private set; }
    public void SaveInventoryData(InventoryStorageData inventoryData)
    {
        InventoryData = inventoryData;
    }
}
