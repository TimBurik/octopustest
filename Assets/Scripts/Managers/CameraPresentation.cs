﻿using UnityEngine;

public class CameraPresentation : MonoBehaviour
{
    private InteractableDetector _interactableDetector;
    public InteractableDetector InteractableDetector
    {
        get
        {
            if (_interactableDetector == null)
            {
                _interactableDetector = GetComponentInChildren<InteractableDetector>();
            }
            return _interactableDetector;
        }
    }

    private Camera _mainCamera;
    public Camera MainCamera
    {
        get
        {
            if (_mainCamera == null)
            {
                _mainCamera = GetComponentInChildren<Camera>();
            }
            return _mainCamera;
        }
    }

    private CameraLook _movementController;
    public CameraLook MovementController
    {
        get
        {
            if (_movementController == null)
            {
                _movementController = GetComponent<CameraLook>();
            }
            return _movementController;
        }
    }
}
