﻿using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerPresentation : MonoBehaviour
{
    private Transform _transform;
    public Transform Transform
    {
        get
        {
            if (_transform == null)
            {
                _transform = GetComponent<Transform>();
            }
            return _transform;
        }
    }

    private ThirdPersonCharacter _movementController;
    public ThirdPersonCharacter MovementController
    {
        get
        {
            if (_movementController == null)
            {
                _movementController = GetComponent<ThirdPersonCharacter>();
            }
            return _movementController;
        }
    }

    private Inventory _inventory;
    public Inventory Inventory
    {
        get
        {
            if (_inventory == null)
            {
                _inventory = GetComponent<Inventory>();
            }
            return _inventory;
        }
    }
}
