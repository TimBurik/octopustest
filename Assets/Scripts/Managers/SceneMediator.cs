﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SceneMediator : Singleton<SceneMediator>
{
    public PlayerPresentation Player;
    public CameraPresentation FollowCamera;
    public UIPresentation UI;
    public Transform LocationRoot;

    private readonly Dictionary<Type, MonoBehaviour> _managersCache = new Dictionary<Type, MonoBehaviour>();

    public TManager GetManager<TManager>() where TManager : MonoBehaviour
    {
        if (!_managersCache.ContainsKey(typeof(TManager)))
        {
            TManager manager = transform.root.GetComponentInChildren<TManager>();
            if (manager != null)
            {
                _managersCache.Add(typeof(TManager), manager);
            }
            else
            {
                Debug.LogError("Cannot find manager of type " + typeof(TManager));
                return null;
            }
        }

        return _managersCache[typeof(TManager)] as TManager;
    }
}
