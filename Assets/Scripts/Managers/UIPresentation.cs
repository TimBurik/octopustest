﻿using UnityEngine;

public class UIPresentation : MonoBehaviour
{
    private CursorInfo _cursorInfo;
    public CursorInfo CursorInfo
    {
        get
        {
            if (_cursorInfo == null)
            {
                _cursorInfo = GetComponentInChildren<CursorInfo>();
            }
            return _cursorInfo;
        }
    }

    private InventoryInfo _inventoryInfo;
    public InventoryInfo InventoryInfo
    {
        get
        {
            if (_inventoryInfo == null)
            {
                _inventoryInfo = GetComponentInChildren<InventoryInfo>();
            }
            return _inventoryInfo;
        }
    }
}
