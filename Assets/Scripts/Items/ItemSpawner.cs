﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public List<GameObject> ItemPrefabs;
    public List<ItemSpawnPoint> ItemSpawnPoints;

    private Dictionary<DecalBase, ItemSpawnPoint> _usedSpawnPoints;
    private List<ItemSpawnPoint> _freeSpawnPoints;

    public void Init()
    {
        _usedSpawnPoints = new Dictionary<DecalBase, ItemSpawnPoint>();
        _freeSpawnPoints = new List<ItemSpawnPoint>(ItemSpawnPoints);
    }

    public void SpawnRandomItems(int amount)
    {
        for (int i = 0; i < amount && _freeSpawnPoints.Count > 0; i++)
        {
            ItemSpawnPoint spawnPoint = _freeSpawnPoints.GetRandomElement();
            DecalBase spawnedItem = SceneMediator.Instance.GetManager<DecalPool>()
                .Get(ItemPrefabs.GetRandomElement(), spawnPoint.Position, spawnPoint.Rotation);

            spawnedItem.OnReleased += OnItemReleased;

            _freeSpawnPoints.Remove(spawnPoint);
            _usedSpawnPoints.Add(spawnedItem, spawnPoint);
        }
    }

    private void OnItemReleased(DecalBase item)
    {
        _freeSpawnPoints.Add(_usedSpawnPoints[item]);
        _usedSpawnPoints.Remove(item);
    }

    public List<ItemSpawnPoint> FindSpawnPointsInLoaction()
    {
        return SceneMediator.Instance.LocationRoot.GetComponentsInChildren<ItemSpawnPoint>().ToList();
    }
}
