﻿using UnityEngine;

public class PickableItem : DecalBase, IInteractable
{
    public ItemType Type;

    public Renderer MainRenderer;
    public Material Normal;
    public Material Hover;

    private MaterialSwapHover _hoverLogic;

    void Start()
    {
        _hoverLogic = new MaterialSwapHover(MainRenderer, Normal, Hover);
    }

    public string HintMessage
    {
        get { return "[RClick] to pickup"; }
    }

    public void Interact()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnItemPickedUp(this);
        Release();
    }

    public void OnHoverEnter()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnItemHoverEnter(this);

        _hoverLogic.OnHoverEnter();
    }

    public void OnHoverExit()
    {
        SceneMediator.Instance.GetManager<GameLogic>().OnItemHoverExit(this);

        _hoverLogic.OnHoverExit();
    }
}
