﻿using UnityEngine;

public class MaterialSwapHover
{
    private readonly Renderer _mainRenderer;
    private readonly Material _normal;
    private readonly Material _hover;

    public MaterialSwapHover(Renderer renderer, Material normal, Material hover)
    {
        _mainRenderer = renderer;
        _normal = normal;
        _hover = hover;
    }

    public void OnHoverEnter()
    {
        _mainRenderer.material = _hover;
    }

    public void OnHoverExit()
    {
        _mainRenderer.material = _normal;
    }
}
