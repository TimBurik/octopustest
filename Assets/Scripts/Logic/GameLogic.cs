﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    public int SpawnItemsOnStart;
    public int SceneToLoad;

    void Start()
    {
        SceneMediator.Instance.Player.Inventory.Init();
        SceneMediator.Instance.GetManager<ItemSpawner>().Init();

        SceneMediator.Instance.GetManager<ItemSpawner>().SpawnRandomItems(SpawnItemsOnStart);
        RedrawInventoryStats();
    }

    public void OnItemHoverEnter(PickableItem item)
    {
        SceneMediator.Instance.UI.CursorInfo.ShowInfoText(item.HintMessage);
    }

    public void OnItemHoverExit(PickableItem item)
    {
        SceneMediator.Instance.UI.CursorInfo.HideInfoText();
    }

    public void OnItemPickedUp(PickableItem item)
    {
        SceneMediator.Instance.Player.Inventory.AddItem(item.Type, 1);
        RedrawInventoryStats();

        SceneMediator.Instance.GetManager<ItemSpawner>().SpawnRandomItems(1);
    }

    public void OnActionInput()
    {
        IInteractable currentInteractable =
            SceneMediator.Instance.FollowCamera.InteractableDetector.DetectedInteractable;
        if (currentInteractable != null)
        {
            currentInteractable.Interact();
        }
    }

    public void OnDoorHoverEnter(Door door)
    {
        List<ICheckResult> doorOpenConditionsCheck = door.OpenConditions.Select(condition => condition.Check()).ToList();
        if (doorOpenConditionsCheck.All(checkResult => checkResult.Satisfied))
        {
            SceneMediator.Instance.UI.CursorInfo.ShowInfoText(door.HintMessage);
        }
        else
        {
            StringBuilder infoBuilder = new StringBuilder("Door Is Locked\n");
            foreach (ICheckResult result in doorOpenConditionsCheck)
            {
                if (!result.Satisfied)
                {
                    infoBuilder.Append(result.Message);
                    infoBuilder.Append("\n");
                }
            }
            SceneMediator.Instance.UI.CursorInfo.ShowInfoText(infoBuilder.ToString());
        }
    }

    public void OnDoorHoverExit(Door door)
    {
        SceneMediator.Instance.UI.CursorInfo.HideInfoText();
    }

    public void OnDoorOpened(Door door)
    {
        foreach (PlayerHasItemsCondition condition in door.OpenConditions)
        {
            SceneMediator.Instance.Player.Inventory.RemoveItem(condition.Type, condition.MinAmount);
        }
        RedrawInventoryStats();
    }

    public void OnDoorEntered(Door door)
    {
        SceneManager.LoadScene(SceneToLoad);
    }

    private void RedrawInventoryStats()
    {
        SceneMediator.Instance.UI.InventoryInfo.RedrawInventoryInfo(SceneMediator.Instance.Player.Inventory);
    }
}
